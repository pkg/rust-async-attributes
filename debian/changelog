rust-async-attributes (1.1.2-4+apertis1) apertis; urgency=medium

  * Switch component from development to target to comply with Apertis
    license policy.
  * Add debian/apertis/copyright.whitelist.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Thu, 16 Jan 2025 11:31:49 +0100

rust-async-attributes (1.1.2-4+apertis0) apertis; urgency=medium

  * Import from Debian bookworm.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Tue, 25 Apr 2023 16:59:37 +0530

rust-async-attributes (1.1.2-4) unstable; urgency=medium

  * fix autopkgtest dependencies

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Feb 2023 17:24:30 +0100

rust-async-attributes (1.1.2-3) unstable; urgency=medium

  * tighten autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Feb 2023 17:14:27 +0100

rust-async-attributes (1.1.2-2) unstable; urgency=medium

  * change binary library package to be arch-independent
  * declare compliance with Debian Policy 4.6.2
  * stop superfluously provide virtual unversioned feature packages
  * update dh-cargo fork;
    build-depend on libstring-shellquote-perl
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Feb 2023 14:57:19 +0100

rust-async-attributes (1.1.2-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * drop patch cherry-picked upstream now applied

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 28 Oct 2022 10:58:46 +0200

rust-async-attributes (1.1.1-2) unstable; urgency=medium

  * enable tests and autopkgtests;
    build-depend on librust-async-std-1+default-dev
  * add patch cherry-picked upstream
    to fix compile error with rustc 1.45.0

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 28 Oct 2022 10:46:54 +0200

rust-async-attributes (1.1.1-1) unstable; urgency=low

  * initial Release;
    closes: Bug#1022141

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 21 Oct 2022 01:13:55 +0200
